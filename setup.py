import setuptools
try:
    from pip._internal.req import parse_requirements
except ImportError:
    from pip.req import parse_requirements

requirements_file = 'requirements.txt'

requires = parse_requirements(requirements_file, session=False)
install_requires = [str(ir.req) for ir in requires]


with open("README.md", "r") as fh:
    long_description = fh.read()


exec(open('brecommend/_version.py').read())


setuptools.setup(
    name="brecommend",
    version=__version__,
    author="William Cole",
    author_email="william.cole@outlook.com",
    description="For recommending machines to players on Bitstarz.",
    url="https://bitbucket.org/wjcole/brecommend/src/master/",
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=install_requires,
)
