'''
This package contains a prototype recommendation engine.  For an input user, it
returns a ranking over the machines he has not played.  The engine is based on
item-item collaborative filtering using the k-nearest-neighbours algorithm.
'''

from ._version import __version__
from .recommender import *
from .data import *
