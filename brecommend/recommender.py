import pandas as pd
import numpy as np
from surprise import Dataset
from surprise import Reader
from surprise import KNNWithMeans
from brecommend import data


class Recommender:

    def __init__(self, df):
        """
        Initialise with Pandas dataframe of aggregated betting data.  Also
        converts machine titles to uppercase to implement case-insensitivity.
        """
        self.machine_cols = ['GAME_TITLE', 'GAME_PROVIDER']
        upper_func = lambda x: x.astype(str).str.upper()
        self.df = df
        self.df[self.machine_cols] = \
            self.df[self.machine_cols].apply(upper_func)

    def aggregate_df_to_machine(self):
        """
        Takes the data from __init__() and aggregates to machine level.
        """
        self.machines = self.df[self.machine_cols].drop_duplicates()
        self.machines['machine_id'] = np.arange(len(self.machines.index))

        gb_cols = ['USER_ID', 'GAME_TITLE', 'GAME_PROVIDER']
        cols_to_drop = ['BET_AMOUNT_IN_CURRENCY', 'WIN_AMOUNT_IN_CURRENCY']
        df_machine = self.df.drop(cols_to_drop, axis=1) \
                            .groupby(gb_cols) \
                            .sum() \
                            .reset_index()
        self.df_machine = pd.merge(df_machine,
                                   self.machines,
                                   on=self.machine_cols,
                                   how='inner')

    def prepare_training_data(self):
        """
        Put the data into a surprise.Dataset object in preparation for fitting.
        """
        max_bet = int(np.ceil(self.df_machine['BET_AMOUNT_IN_EUR'].max()))
        reader = Reader(rating_scale=(1, max_bet))

        relevant_cols = ['USER_ID', 'machine_id', 'BET_AMOUNT_IN_EUR']
        self.data = Dataset.load_from_df(self.df_machine[relevant_cols],
                                         reader)

    def set_options(self, sim_options=None):
        """
        Provide a dictionary of sim_options to feed into surprise.KNNWithMeans.
        Chooses item-item filtering and mean-square distance by default.
        """
        # To use item-based mean-square distance similarity
        sim_options_default = {
            'name': 'msd',
            'user_based': False,  # Compute similarities between items
        }
        sim_options = sim_options or sim_options_default
        self.algo = KNNWithMeans(sim_options=sim_options)

    def fit(self):
        """
        Fit the KNN predictive model.
        """
        training_set = self.data.build_full_trainset()
        self.algo.fit(training_set)

    def predict(self, user_id, machine_id):
        """
        Returns a prediction of the total EUR bet on a machine by a player.
        """
        prediction = self.algo.predict(uid=user_id, iid=machine_id)
        return prediction.est

    def return_recommendation(self, user_id):
        """
        Return the top_x machines for a player.  Default: 10.
        """
        if user_id not in self.df_machine['USER_ID'].to_list():
            raise ValueError(f'USER_ID {user_id} not in data.')
        played_machines = self.df_machine.query('USER_ID == @user_id')\
            ['machine_id'].drop_duplicates()
        unplayed_machines = [x for x in self.machines['machine_id']
                             if x not in played_machines.to_list()]
        pred_bet = [self.predict(user_id, x) for x in unplayed_machines]
        rec_df = pd.DataFrame({'machine_id': unplayed_machines,
                               'predicted_bet': pred_bet})
        rec_df['recommendation_rank'] = rec_df['predicted_bet'].rank(
                method='first',
                ascending=False)
        rec_df = rec_df.merge(self.machines,
                              on='machine_id',
                              how='inner').sort_values('recommendation_rank')

        return rec_df[['GAME_TITLE',
                       'GAME_PROVIDER',
                       'machine_id',
                       'predicted_bet',
                       'recommendation_rank']]

    def run_all_steps(self, sim_options=None):
        """
        Runs all ranking steps aside from returning the recommendations.
        """
        self.aggregate_df_to_machine()
        self.prepare_training_data()
        self.set_options(sim_options=sim_options)
        self.fit()


if __name__ == '__main__':
    user_id_example = 5
    print('Importing sample data...')
    df = data.load_sample_data()
    print('Forming recommendations...')
    rec = Recommender(df)
    rec.run_all_steps()
    advice = rec.return_recommendation(user_id=user_id_example)
    print("Object 'advice' contains ranking of unplayed machines for player "
          f"{user_id_example}.")
