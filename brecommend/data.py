import pandas as pd
from pathlib import Path


def load_sample_data():
    '''
    Returns the sample data stored in the repository as a Pandas dataframe.
    '''
    file = 'bitstarz_recommendation_engine_data.csv'
    path = Path(__file__).parent / file
    df = load_data(path)
    return df


def load_data(path):
    '''
    Directed at a CSV of the data extract, will load into a Pandas dataframe
    and return.  Uses pd.read_csv() with the correct settings.
    '''
    df = pd.read_csv(path, delimiter='\t', encoding = "ISO-8859-1")
    return df
