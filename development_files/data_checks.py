#%%% Libraries

import pandas as pd
import numpy as np

#%%

file = '/Users/wjcole/Documents/personal/CV/2020-07-23_bitstarz/repo/bitstarz_recommendation_engine_data.csv'
df = pd.read_csv(file, delimiter='\t', encoding = "ISO-8859-1")


#%% Distinctness checks

cols = ['USER_ID', 'CURRENCY', 'GAME_TITLE', 'GAME_PROVIDER']

x = df[cols].duplicated().sum()
print(x)

#%% Game details

cols = ['GAME_TITLE', 'GAME_PROVIDER']
df[cols].drop_duplicates().sort_values(cols)

#%% Aggregate to EUR level


df_machine = df.groupby(['USER_ID', 'GAME_TITLE', 'GAME_PROVIDER']).sum().reset_index()
df_machine.isna().sum()

pairs = df_machine[['GAME_TITLE', 'GAME_PROVIDER']].drop_duplicates().reset_index(drop=True)
pairs.index.set_names('machine_id', inplace=True)
pairs.reset_index(inplace=True)

df_machine2 = pd.merge(df_machine, pairs, on=['GAME_TITLE', 'GAME_PROVIDER'], how='inner')


#%%

import pandas as pd
from surprise import Dataset
from surprise import Reader

# This is the same data that was plotted for similarity earlier
# with one new user "E" who has rated only movie 1
#ratings_dict = {
#    "item": [1, 2, 1, 2, 1, 2, 1, 2, 1],
#    "user": ['A', 'A', 'B', 'B', 'C', 'C', 'D', 'D', 'E'],
#    "rating": [1, 2, 2, 4, 2.5, 4, 4.5, 5, 3],
#}

#df = pd.DataFrame(ratings_dict)
reader = Reader(rating_scale=(1, int(np.ceil(df_machine['BET_AMOUNT_IN_EUR'].max()))))

# Loads Pandas dataframe
data = Dataset.load_from_df(df_machine2[['USER_ID', 'machine_id', 'BET_AMOUNT_IN_EUR']], reader)

#%%


from surprise import KNNWithMeans

# To use item-based cosine similarity
sim_options = {
    "name": "cosine",
    "user_based": False,  # Compute  similarities between items
}
algo = KNNWithMeans(sim_options=sim_options)

#%%

trainingSet = data.build_full_trainset()
algo.fit(trainingSet)
prediction = algo.predict(2583592, 0)
prediction.est



