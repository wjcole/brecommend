# Installation instructions

* Clone the repository.

* Navigate to the repository root directory.

* Run `pip install .`

* Check installation is present using `pip list`.

* The package may need to be made reachable to the Python
interpreter.  This is done by adding the repository directory to the
`$PYTHONPATH` environment variable.

On a Mac, the final step can be done by adding the following line to
`~/.bash_profile`.
```
export PYTHONPATH="$PYTHONPATH:<repository parent directory>"
```


# Usage instructions

Sample running code is given in the automatic run section at the bottom of
`brecommend/recommend.py`.


# About this package

Built as a prototype for the Bitstarz challenge.

William Cole, July 2020
