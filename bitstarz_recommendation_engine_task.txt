Hi William,

We would like to congratulate you on successful passing first interview round and advancing to the second phase, where you will be challenged with more hands-on task. 
The goal is to solve the "Player game recommendation engine" challenge, which is described below.

We expect you to have a presentation with your solution.
In addition to technical implementation, it is also important that you effectively defend your methodology and findings.



"Player game recommendation engine" challenge

Bitstarz is an online casino where players can play different types of online games (slot, card, roulette, poker, ...).
Bitstarz is constantly monitoring the behaviour of its users and is introducing continuous improvements to its online casino in order to provide the best possible user experience.

Players can choose, among other things, the currency in which they place their bets.
In addition to classic FIAT currencies (EUR, USD, AUD, ...), they can also bet with cryptocurrencies (BTC, EUR, ...).

Players have more than 2000 different games at their disposal, with a steadily increasing number of games.
Due to the amount of games, the player is faced with the problem of how to choose the game that is most suitable for him.
This is where Bitstarz wants to help the player, and with the help of your "Player game recommendation engine" algorithm, suggest a list of games that will work best for him.

To solve the problem, you will use sample of aggregated data on anonymous bets made by players.
Aggregated data are: number of bets, winnings and amounts.
In addition, the category of the game, its title and the casino supplier have been added.

Your task is to come up with an algorithm that will suggest players the games they are most likely to like, which means that they will continue to be happy to bet. At the same time, the client (casino management) would like to know what key your program offers games to players and why, and what games to offer their players in the future.

Attached to the email you'll find a compressed "bitstarz_recommendation_engine_data.zip" file that contains over a million data rows. The file is in the form of a ".csv" and contains representative data sample from October 2018 till October 2019.

If you have any questions, please contact us by email, also to suggest your prefered presentation date.

Much joy in exploring this topic and success in implementation and presentation,


Looking forward to meeting you,


Dejan
